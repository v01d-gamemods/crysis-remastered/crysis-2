from trainerbase.codeinjection import CodeInjection
from trainerbase.memory import Address, pm


freeze_ammo = CodeInjection(
    pm.base_address + 0x11FD657,
    b"\x90" * 3,
)

freeze_energy = CodeInjection(
    pm.base_address + 0x131F76F,
    b"\x90" * 4,
)
