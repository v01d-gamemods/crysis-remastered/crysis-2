run:
	pdm run python app/main.py

format:
	pdm run black .

lint: format
	pdm run pylint app
